#include <iostream>
using namespace std;

void func(int * arr){
    int i = 0;
    int cnt = 0;
    while(arr[i]){
        cnt++;
        i++;    
    }
    cout<<cnt;
}

int main(){
    int arr[5] = {1,2,3,4,5};
    func(arr);
    return 0;
}